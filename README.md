# README #
This payment driver is designed to receive payment orders for CMS Amoro

=== Wallet One Checkout ===
Contributors: h-elena
Version: 1.1
Requires at least: 7.0.0.14
Tested up to: 7.0.0.14
Stable tag: 7.0.0.14
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Checkout module is a payment system for CMS Amiro.

== Description ==
If you have an online store, then you need a module payments for orders made.

== Installation ==
1. Register on the site https://www.walletone.com/merchant/client/#/signup
2. Download the payment driver on the admin panel your site.
3. Instructions for configuring the payment driver is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==


= 1.1.0 = 
* Change expired date for invoice(30 days)


== Frequently Asked Questions ==
No recent asked questions
